// JavaScript Document
var checkForWinBool = false;
var recurse  = new Array();
var used = new Array();
var mins,secs,TimerRunning,TimerID;
TimerRunning=false;

$(document).ready(function(e) {
	$('#levels').click(function(){
		$('#levelDetails').slideDown(500); 
	});
});
	
function makeMine(rows,cols,bombs)
{
	var arr = new Array(rows>cols?rows:cols);
	var oldFlagVal = new Array();
	mins=0;
	secs=0;
	StopTimer();
	$('#timeshow').html('00:00');
	$('#main').html('');
	$('#levelDetails').slideUp(100);
	$('#customDiv').slideUp(200);
	$('#bombsRemaining').html(bombs);
	
	for(var r = 1 ; r <= rows ; r++)
	{
		arr[r] = new Array(cols);
		for(var c = 1 ; c <= cols ; c++)
		{
			arr[r][c] = 0;
			$('<div class="blocks" id="row'+r+'col'+c+'">&nbsp;</div>').appendTo('#main');
		}
		$('<br>').appendTo('#main');
	}
	
	//making bombs
	var bomb = new Array()
	for(var b = 1 ; b <= bombs ; b++)
	{
		var ran = Math.floor((Math.random()*rows*cols)+2);
		var posx = Math.floor(ran/cols)==0?1:Math.floor(ran/cols);
		var posy = (ran%(cols+1)==0?1:ran%(cols+1));
		if(arr[posx][posy] == -2)
			b--;
		else
		{
			arr[posx][posy] = -2;
			bomb.push(posx);
			bomb.push(posy);
			$('#row'+posx+'col'+posy).html('<span class="hide" id="span_row'+posx+'col'+posy+'"><img src="resources/images/bomb.png"/></span>');
			var xminus = posx-1;
			var xplus = posx+1;
			var yminus = posy-1;
			var yplus = posy+1;
			if(arr[xminus] != undefined)
			{
				if(arr[xminus][yminus] != -2)
				{
					arr[xminus][yminus] += 1;
					$('#row'+xminus+'col'+yminus).html('<span class="hide" id="span_row'+xminus+'col'+yminus+'">'+arr[xminus][yminus]+'</span>');
				}
				
				if(arr[xminus][posy] != -2)
				{
					arr[xminus][posy] += 1;
					$('#row'+xminus+'col'+posy).html('<span class="hide" id="span_row'+xminus+'col'+posy+'">'+arr[xminus][posy]+'</span>');
				}
				
				if(arr[xminus][yplus] != -2)
				{
					arr[xminus][yplus] += 1;
					$('#row'+xminus+'col'+yplus).html('<span class="hide" id="span_row'+xminus+'col'+yplus+'">'+arr[xminus][yplus]+'</span>');
				}
			}
			if(arr[posx] != undefined)
			{
				if(arr[posx][yplus] != -2)
				{
					arr[posx][yplus] += 1;
					$('#row'+posx+'col'+yplus).html('<span class="hide" id="span_row'+posx+'col'+yplus+'">'+arr[posx][yplus]+'</span>');
				}
			}
			if(arr[xplus] != undefined)
			{
				if(arr[xplus][yplus] != -2)
				{
					arr[xplus][yplus] += 1;
					$('#row'+xplus+'col'+yplus).html('<span class="hide" id="span_row'+xplus+'col'+yplus+'">'+arr[xplus][yplus]+'</span>');
				}
				
				if(arr[xplus][posy] != -2)
				{
					arr[xplus][posy] += 1;
					$('#row'+xplus+'col'+posy).html('<span class="hide" id="span_row'+xplus+'col'+posy+'">'+arr[xplus][posy]+'</span>');
				}
				
				if(arr[xplus][yminus] != -2)
				{
					arr[xplus][yminus] += 1;
					$('#row'+xplus+'col'+yminus).html('<span class="hide" id="span_row'+xplus+'col'+yminus+'">'+arr[xplus][yminus]+'</span>');
				}
			}
			if(arr[posx] != undefined)
			{
				if(arr[posx][yminus] != -2)
				{
					arr[posx][yminus] += 1;
					$('#row'+posx+'col'+yminus).html('<span class="hide" id="span_row'+posx+'col'+yminus+'">'+arr[posx][yminus]+'</span>');
				}
			}
		}
	}
	
	$('.blocks').mousedown(function(event) {
		var id = $(this).attr('id');
		var indexOfRow = id.indexOf('row');
		var indexOfCol = id.indexOf('col');
		var x = parseInt(id.substring(3,indexOfCol));
		var y = parseInt(id.substring(indexOfCol+3));
		
		if(!TimerRunning)
			Init();
		switch (event.which) {
			case 1:
				{
					//Left mouse button clicked
					if(arr[x][y] != -3)
					{
						if(arr[x][y] == -2)
						{
							for(var i = 0 ; i < bomb.length ; i=i+2)
							{
								$('#row'+bomb[i]+'col'+bomb[i+1]).css({'background-image':'none'});
								$('#span_row'+bomb[i]+'col'+bomb[i+1]).removeClass('hide');	
							}
							StopTimer();
							alert('Sorry you lost the game!!')
						}
						else if(arr[x][y] != 0)
						{	
							$('#row'+x+'col'+y).css({'background-image':'none'});
							$('#span_row'+x+'col'+y).removeClass('hide');
							if(checkForWinBool)
								checkForWin(rows,cols,arr,bombs);
						}
						
						else
						{
							$('#row'+x+'col'+y).css({'background-image':'none'});
							$('#row'+x+'col'+y).html('&nbsp;');
							do{
								if(checkForWinBool)
									checkForWin(rows,cols,arr,bombs);
								checkAdjacent(x,y,rows,cols,arr);
								x = recurse[0];
								y = recurse[1];
								recurse.shift();
								recurse.shift();
							}while(recurse.length > 0);
						}	
					}
					break;
				}
			case 2:
				{
					//Middle mouse button clicked
					break;
				}
			case 3:
				{
					//Right mouse button clicked
					if( arr[x][y] == -3)
					{
						var o = oldFlagVal.indexOf(x+":"+y);
						if(o >= 0)
						{
							var xaxis = oldFlagVal[o].substring(0,oldFlagVal[o].indexOf(':'));
							var yaxis = oldFlagVal[o].substring(oldFlagVal[o].indexOf(':')+1);
							if(isNaN(oldFlagVal[o+1]))
								arr[xaxis][yaxis] = 0;
							else
								arr[xaxis][yaxis] = parseInt(oldFlagVal[o+1]);
							$('#row'+x+'col'+y).css({'background-image':'url(resources/images/blueDiv.png)'});
							oldFlagVal.splice(o,2);
							$('#bombsRemaining').html(parseInt($('#bombsRemaining').html())+1);
						}
						else
							alert('ERROR. Please refresh and play again!!')
					}
					else
					{
						if(parseInt($('#bombsRemaining').html()) > 0)
						{
							$('#bombsRemaining').html(parseInt($('#bombsRemaining').html())-1);
							oldFlagVal.push(x+':'+y);
							oldFlagVal.push(arr[x][y]);
							$('#row'+x+'col'+y).css({'background-image':'url(resources/images/flag.png)'});
							arr[x][y] = -3;	
							if(parseInt($('#bombsRemaining').html()) == 0)
								checkForWin(rows,cols,arr,bombs);
						}
						else
							alert('No more bombs to be found!!!');
					}
					break;
				}
			default:
				alert('You have a strange mouse');
		}
	});
}

function checkForWin(rows,cols,arr,bombs)
{
	checkForWinBool = true;
	var win = true;
	var hideCount = 0;
	for(var r = 1 ; r <= rows ; r++)
	{
		for(var c = 1 ; c <= cols ; c++)
		{
			if($('#span_row'+r+'col'+c).hasClass('hide'))
			{
				hideCount = hideCount + 1;
				if(hideCount > bombs)
				{
					win=false;
					break;
				}
			}
			if(arr[r][c] == -2)
			{
				win=false;
				break;
			}
		}
		if(!win)
			break;
	}
	if(win)
	{
		StopTimer();
		var r=confirm("Congratulations!!! You won in "+$('#timeshow').html()+" mins.\n Start a new game?")
		if (r==true)
		{
			makeMine(9,9,3);
		}
	}
}

function checkAdjacent(x,y,rows,cols,arr)
{
	var xminus = x-1<1?1:x-1;
	var xplus = x+1>rows?rows:x+1;
	var yminus = y-1<1?1:y-1;
	var yplus = y+1>cols?cols:y+1;
	 
	var one = arr[xminus][yminus];
	var two = arr[xminus][y];
	var three = arr[xminus][yplus];
	var four = arr[x][yplus];
	var five = arr[xplus][yplus];
	var six = arr[xplus][y];
	var seven = arr[xplus][yminus];
	var eight = arr[x][yminus];
	
	if(one != -3)
	{
		if(one != 0)
		{
			$('#row'+xminus+'col'+yminus).css({'background-image':'none'});
			$('#span_row'+xminus+'col'+yminus).removeClass('hide');
		}
		else
		{
			var t = recurse.indexOf(xminus);
			if(!(t != -1 && recurse[t+1]== yminus) && $('#row'+xminus+'col'+yminus).css('background-image') != 'none')
			{
				recurse.push(xminus);
				recurse.push(yminus);
				$('#row'+xminus+'col'+yminus).html('&nbsp;');
			}
			$('#row'+xminus+'col'+yminus).css({'background-image':'none'});
		}
	}
	
	if(two != -3)
	{
		if(two != 0)
		{
			$('#row'+xminus+'col'+y).css({'background-image':'none'});
			$('#span_row'+xminus+'col'+y).removeClass('hide');
		}
		else
		{
			var t = recurse.indexOf(xminus);
			if(!(t != -1 && recurse[t+1]== y) && $('#row'+xminus+'col'+y).css('background-image') != 'none')
			{
				recurse.push(xminus);
				recurse.push(y);
				$('#row'+xminus+'col'+y).html('&nbsp;');
			}
			$('#row'+xminus+'col'+y).css({'background-image':'none'});
		}
	}
	
	if(three != -3)
	{
		if(three != 0)
		{
			$('#row'+xminus+'col'+yplus).css({'background-image':'none'});
			$('#span_row'+xminus+'col'+yplus).removeClass('hide');
		}
		else
		{
			var t = recurse.indexOf(xminus);
			if(!(t != -1 && recurse[t+1]== yplus) && $('#row'+xminus+'col'+yplus).css('background-image') != 'none')
			{
				recurse.push(xminus);
				recurse.push(yplus);
				$('#row'+xminus+'col'+yplus).html('&nbsp;');
			}
			$('#row'+xminus+'col'+yplus).css({'background-image':'none'});
		}
	}
	
	if(four != -3)
	{
		if(four != 0)
		{
			$('#row'+x+'col'+yplus).css({'background-image':'none'});
			$('#span_row'+x+'col'+yplus).removeClass('hide');
		}
		else
		{
			var t = recurse.indexOf(x);
			if(!(t != -1 && recurse[t+1]== yplus) && $('#row'+x+'col'+yplus).css('background-image') != 'none')
			{
				recurse.push(x);
				recurse.push(yplus);
				$('#row'+x+'col'+yplus).html('&nbsp;');
			}
			$('#row'+x+'col'+yplus).css({'background-image':'none'});
		}
	}
	
	if(five != -3)
	{
		if(five != 0)
		{
			$('#row'+xplus+'col'+yplus).css({'background-image':'none'});
			$('#span_row'+xplus+'col'+yplus).removeClass('hide');
		}
		else
		{
			var t = recurse.indexOf(xplus);
			if(!(t != -1 && recurse[t+1]== yplus) && $('#row'+xplus+'col'+yplus).css('background-image') != 'none')
			{
				recurse.push(xplus);
				recurse.push(yplus);
				$('#row'+xplus+'col'+yplus).html('&nbsp;');
			}
			$('#row'+xplus+'col'+yplus).css({'background-image':'none'});
		}
	}
	
	if(six != -3)
	{
		if(six != 0)
		{
			$('#row'+xplus+'col'+y).css({'background-image':'none'});
			$('#span_row'+xplus+'col'+y).removeClass('hide');
		}
		else
		{
			var t = recurse.indexOf(xplus);
			if(!(t != -1 && recurse[t+1]== y) && $('#row'+xplus+'col'+y).css('background-image') != 'none')
			{
				recurse.push(xplus);
				recurse.push(y);
				$('#row'+xplus+'col'+y).html('&nbsp;');
			}
			$('#row'+xplus+'col'+y).css({'background-image':'none'});
		}
	}
	
	if(seven != -3)
	{
		if(seven != 0)
		{
			$('#row'+xplus+'col'+yminus).css({'background-image':'none'});
			$('#span_row'+xplus+'col'+yminus).removeClass('hide');
		}
		else
		{
			var t = recurse.indexOf(xplus);
			if(!(t != -1 && recurse[t+1]== yminus) && $('#row'+xplus+'col'+yminus).css('background-image') != 'none')
			{
				recurse.push(xplus);
				recurse.push(yminus);
				$('#row'+xplus+'col'+yminus).html('&nbsp;');
			}
			$('#row'+xplus+'col'+yminus).css({'background-image':'none'});
		}
	}
	
	if(eight != -3)
	{
		if(eight != 0)
		{
			$('#row'+x+'col'+yminus).css({'background-image':'none'});
			$('#span_row'+x+'col'+yminus).removeClass('hide');
		}
		else
		{
			var t = recurse.indexOf(x);
			if(!(t != -1 && recurse[t+1]== yminus) && $('#row'+x+'col'+yminus).css('background-image') != 'none')
			{
				recurse.push(x);
				recurse.push(yminus);
				$('#row'+x+'col'+yminus).html('&nbsp;');
			}
			$('#row'+x+'col'+yminus).css({'background-image':'none'});
		}
	}
}
	
function Init() //call the Init function when u need to start the timer
{
	//mins=0;
	//secs=0;
	//StopTimer();
	StartTimer();
}

function StopTimer()
{
	if(TimerRunning)
	   clearTimeout(TimerID);
	TimerRunning=false;
}

function StartTimer()
{
	TimerRunning=true;
	$('#timeshow').html(Pad(mins)+":"+Pad(secs));
	TimerID=self.setTimeout("StartTimer()",1000);
	
	if(mins==60)
	   StopTimer();
	
	if(secs==60)
	{
	   mins++;
	   secs=00;
	}
	secs++;

}

function Pad(number) //pads the mins/secs with a 0 if its less than 10
{
	if(number<10)
	   number=0+""+number;
	return number;
}

function choseCustom()
{
	$('#customDiv').slideDown(500); 
}

function makeCustom()
{
	var msg = "";
	var r1 = parseInt($('#rows').val());
	var c1 = parseInt($('#cols').val());
	var b1 = parseInt($('#bombs').val());
	
	if(isNaN(r1) || r1 == '')
		msg += "Please check rows. Either its blank or Not a valid number";
	if(isNaN(c1) || c1 == '')
		msg += "Please check cols. Either its blank or Not a valid number/n";
	if(isNaN(b1) || b1 == '')
		msg += "Please check bombs. Either its blank or Not a valid number/n";
	
	if(msg == '')
		makeMine(r1,c1,b1);
	else
		alert(msg);		 
}